import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class GridPanel extends JPanel
{
    private static final int panelMaxWidth = 1080;
    private static final int panelMaxHeight = 768;

    private int rows;
    private int cols;
    private Cell[][] cellGrid;
    private GridEngine engine;

    public Cell getCell(int row, int col)
    {
        return cellGrid[row][col];
    }
    private int getOptimalCellSize(int rowCount,int colCount)
    {
        int horMaxCellSize = panelMaxWidth/colCount;
        int vertMaxCellSize = panelMaxHeight/rowCount;
        int finalCellSize = 64;
        if(horMaxCellSize < finalCellSize)
            finalCellSize = horMaxCellSize;
        if(vertMaxCellSize < finalCellSize)
            finalCellSize = vertMaxCellSize;
        if(finalCellSize < 48)
            finalCellSize = 48;
        return finalCellSize;
    }
    public GridPanel(GridEngine engineObject)
    {
        engine = engineObject;
        rows = engine.getRowCount();
        cols = engine.getColCount();
        cellGrid = new Cell[rows][cols];
        int optimalCellSize = getOptimalCellSize(rows,cols);
        GridLayout layout = new GridLayout(rows,cols);
        setLayout(layout);
        for(int row = 0;row<rows;row++)
            for(int col = 0;col<cols;col++)
            {
                cellGrid[row][col] = new Cell(row,col,engine.getGridValue(row,col),optimalCellSize, engine);

                //Set border
                int top=1, left=1, bot=0, right=0;
                if(row == rows-1)
                    bot = 1;
                if(col == cols-1)
                    right = 1;
                Border border = new MatteBorder(top,left,bot,right,Color.DARK_GRAY);
                cellGrid[row][col].setBorder(border);

                add(cellGrid[row][col]);
            }

    }

    public void revealBombs()
    {
        int gridRows = engine.getRowCount();
        int gridCols = engine.getColCount();
        for(int row=0;row<gridRows;row++)
            for(int col=0;col<gridCols;col++)
            {
                if(engine.isMarkedWrong(row,col))
                    cellGrid[row][col].showMarkedWrong();
                if(engine.isUnmarkedBomb(row,col))
                    cellGrid[row][col].showBombRevealed();
            }
    }

    public void disableAllCells()
    {
        for(int row = 0;row<rows;row++)
            for(int col = 0;col<cols;col++)
                cellGrid[row][col].removeMouseListener(cellGrid[row][col].getClickDetector());
    }
}
