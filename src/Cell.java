import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

public class Cell extends JPanel
{
    private static Image imageCellCovered;
    private static Image imageCellBomb;
    private static Image imageCellBombRevealed;
    private static Image imageCellMarked;
    private static Image imageCellMarkedWrong;
    private static final java.awt.Color[] colorList = new java.awt.Color[9];
    static
    {
        colorList[0] = new java.awt.Color(255,255,255);
        colorList[1] = new java.awt.Color(0,0,255);
        colorList[2] = new java.awt.Color(0,170,0);
        colorList[3] = new java.awt.Color(255,0,0);
        colorList[4] = new java.awt.Color(0,0,100);
        colorList[5] = new java.awt.Color(120,0,0);
        colorList[6] = new java.awt.Color(0,180,180);
        colorList[7] = new java.awt.Color(0,0,0);
        colorList[8] = new java.awt.Color(100,100,100);
        /*try
        {*/
            /*imageCellCovered = ImageIO.read(new File("CellCovered.png"));
            imageCellBomb = ImageIO.read(new File("CellBomb.png"));
            imageCellBombRevealed = ImageIO.read(new File("CellBombRevealed.png"));
            imageCellMarked = ImageIO.read(new File("CellMarked.png"));
            imageCellMarkedWrong = ImageIO.read(new File("CellMarkedWrong.png"));*/
            imageCellCovered = Toolkit.getDefaultToolkit().createImage(Cell.class.getResource("CellCovered.png"));
            imageCellBomb = Toolkit.getDefaultToolkit().createImage(Cell.class.getResource("CellBomb.png"));
            imageCellBombRevealed = Toolkit.getDefaultToolkit().createImage(Cell.class.getResource("CellBombRevealed.png"));
            imageCellMarked = Toolkit.getDefaultToolkit().createImage(Cell.class.getResource("CellMarked.png"));
            imageCellMarkedWrong = Toolkit.getDefaultToolkit().createImage(Cell.class.getResource("CellMarkedWrong.png"));

        /*}*/
        /*catch (IOException e)
        {
            System.out.println("RIP IMAGES");
        }*/
    }

    private String currentCard;
    private CardLayout cards;
    private MouseAdapter clickDetector;

    private void toggleMarked(boolean markedValue)
    {
        if(markedValue)
        {
            cards.show(Cell.this,"MARKED");
            currentCard = "MARKED";
        }
        else
        {
            cards.show(Cell.this, "COVERED");
            currentCard = "COVERED";
        }
    }

    public MouseAdapter getClickDetector(){return clickDetector;}
    public void explode()
    {
        showValue();
        setBackground(Color.RED);
    }
    public void showMarkedWrong()
    {
        cards.show(Cell.this, "MARKED WRONG");
    }
    public void showBombRevealed()
    {
        cards.show(Cell.this, "BOMB REVEALED");
    }
    public void showValue()
    {
            cards.show(Cell.this, "VALUE");
            currentCard = "VALUE";
    }

    public Cell(int cellRow, int cellCol, int cellValue, int cellSize, GridEngine engine)
    {
        cards = new CardLayout();
        setLayout(cards);

        //Set up the Value Label
        JLabel valueLabel = new JLabel();
        switch(cellValue)
        {
            case -1:
            {
                Icon bombIcon = new ImageIcon(imageCellBomb.getScaledInstance(cellSize,cellSize,Image.SCALE_SMOOTH));
                valueLabel.setIcon(bombIcon);
                break;
            }
            case 0:
            {
                break;
            }
            default:
            {
                valueLabel.setText(Integer.toString(cellValue));
                valueLabel.setFont(new Font("Dialog",Font.BOLD , (int)(cellSize/1.5)));
                valueLabel.setForeground(colorList[cellValue]);
            }
        }
        valueLabel.setHorizontalAlignment(SwingConstants.CENTER);
        valueLabel.setVerticalAlignment(SwingConstants.CENTER);

        //Set up marked label
        JLabel markedLabel = new JLabel();
        Icon markedIcon = new ImageIcon(imageCellMarked.getScaledInstance(cellSize,cellSize,Image.SCALE_SMOOTH));
        markedLabel.setIcon(markedIcon);
        markedLabel.setHorizontalAlignment(SwingConstants.CENTER);
        markedLabel.setVerticalAlignment(SwingConstants.CENTER);

        //Set up covered label
        JLabel coveredLabel = new JLabel();
        Icon coveredIcon = new ImageIcon(imageCellCovered.getScaledInstance(cellSize,cellSize,Image.SCALE_SMOOTH));
        coveredLabel.setIcon(coveredIcon);
        coveredLabel.setHorizontalAlignment(SwingConstants.CENTER);
        coveredLabel.setVerticalAlignment(SwingConstants.CENTER);

        //Set up endgame labels
        JLabel markedWrongLabel = new JLabel();
        JLabel bombRevealedLabel = new JLabel();
        Icon markedWrongIcon = new ImageIcon(imageCellMarkedWrong.getScaledInstance(cellSize,cellSize,Image.SCALE_SMOOTH));
        Icon bombRevealedIcon = new ImageIcon(imageCellBombRevealed.getScaledInstance(cellSize,cellSize,Image.SCALE_SMOOTH));
        markedWrongLabel.setIcon(markedWrongIcon);
        bombRevealedLabel.setIcon(bombRevealedIcon);
        markedWrongLabel.setHorizontalAlignment(SwingConstants.CENTER);
        markedWrongLabel.setVerticalAlignment(SwingConstants.CENTER);
        bombRevealedLabel.setHorizontalAlignment(SwingConstants.CENTER);
        bombRevealedLabel.setVerticalAlignment(SwingConstants.CENTER);

        //Add labels to layout
        add(valueLabel,"VALUE");
        add(markedLabel,"MARKED");
        add(coveredLabel,"COVERED");
        add(markedWrongLabel,"MARKED WRONG");
        add(bombRevealedLabel,"BOMB REVEALED");
        cards.show(this,"COVERED");
        currentCard = "COVERED";

        clickDetector = new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent event)
            {
                if(!currentCard.equals( "VALUE"))
                    if(SwingUtilities.isLeftMouseButton(event))
                    {
                        if(currentCard.equals("COVERED"))
                            engine.openCell(cellRow, cellCol);
                    }
                    else // isRightMouseButton
                    {
                        if(currentCard.equals("COVERED"))
                        {
                            if(engine.getCurrentMineCount() >0)
                            {
                                toggleMarked(true);
                                engine.updateMineCount(engine.getCurrentMineCount() - 1);
                                engine.setUserCell(cellRow,cellCol,-1);
                            }
                        }
                        else
                        {
                            toggleMarked(false);
                            engine.updateMineCount(engine.getCurrentMineCount()+1);
                            engine.setUserCell(cellRow,cellCol,0);
                        }
                    }
            }
        };
        addMouseListener(clickDetector);

        Dimension cellDim = new Dimension(cellSize,cellSize);
        setMinimumSize(cellDim);
        setPreferredSize(cellDim);
        setMaximumSize(cellDim);
    }
}
