import javax.swing.*;
import java.awt.*;
public class MainGUI extends JFrame
{
    private static final int HEIGHT = 768+13;
    private static final int WIDTH = 1080;

    private JTextField inputHeight;
    private JTextField inputMines;
    private JTextField inputWidth;
    private JLabel labelMineCount;
    private JPanel panelMain;
    private JRadioButton rButtonBeginner;
    private JRadioButton rButtonIntermediate;
    private JRadioButton rButtonExpert;
    private JRadioButton rButtonCustom;
    private GridPanel panelGrid;

    private GridEngine engine;
    public void setEngine(GridEngine engineObject)
    {
        engine = engineObject;
    }

    public MainGUI()
    {

        initComponents();
    }

    private void initComponents() {

        ButtonGroup buttonGroupDifficulty;
        JButton buttonStart;
        JLabel labelHeight;
        JLabel labelMines;
        JLabel labelWidth;
        JLabel labelMinesLeft;
        JPanel panelCustomValues;
        JPanel panelMenu;
        JPanel panelOptions;
        JPanel panelMineCount;
        JScrollPane scrollGrid;

        buttonGroupDifficulty = new ButtonGroup();
        panelMenu = new JPanel();
        buttonStart = new JButton();
        panelOptions = new JPanel();
        rButtonBeginner = new JRadioButton();
        rButtonIntermediate = new JRadioButton();
        rButtonExpert = new JRadioButton();
        rButtonCustom = new JRadioButton();
        panelCustomValues = new JPanel();
        panelMineCount = new JPanel();
        labelWidth = new JLabel();
        labelHeight = new JLabel();
        labelMines = new JLabel();
        labelMinesLeft = new JLabel();
        labelMineCount = new JLabel();
        inputWidth = new JTextField();
        inputHeight = new JTextField();
        inputMines = new JTextField();
        panelMain = new JPanel();
        scrollGrid = new JScrollPane();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        buttonStart.setText("START / RESET");
        buttonStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStartActionPerformed(evt);
            }
        });

        buttonGroupDifficulty.add(rButtonBeginner);
        rButtonBeginner.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        rButtonBeginner.setText("Beginner");
        rButtonBeginner.setSelected(true);

        buttonGroupDifficulty.add(rButtonIntermediate);
        rButtonIntermediate.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        rButtonIntermediate.setText("Intermediate");

        buttonGroupDifficulty.add(rButtonExpert);
        rButtonExpert.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        rButtonExpert.setText("Expert");

        buttonGroupDifficulty.add(rButtonCustom);
        rButtonCustom.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        rButtonCustom.setText("Custom");

        labelWidth.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        labelWidth.setText("Width:");

        labelHeight.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        labelHeight.setText("Height:");

        labelMines.setFont(new Font("Dialog", Font.BOLD, 18)); // NOI18N
        labelMines.setText("Mines:");

        labelMinesLeft.setFont(new Font("Dialog", Font.BOLD, 18));
        labelMinesLeft.setText("Mines left:");

        labelMineCount.setFont(new Font("Dialog", Font.BOLD, 18));
        labelMineCount.setText("N/A");

        inputWidth.setText("8");
        inputHeight.setText("8");
        inputMines.setText("1");

        panelCustomValues.setLayout(new GridLayout(3,2));
        panelCustomValues.add(labelWidth);
        panelCustomValues.add(inputWidth);
        panelCustomValues.add(labelHeight);
        panelCustomValues.add(inputHeight);
        panelCustomValues.add(labelMines);
        panelCustomValues.add(inputMines);

        panelOptions.setLayout(new GridLayout(0,1));
        panelOptions.add(rButtonBeginner);
        panelOptions.add(rButtonIntermediate);
        panelOptions.add(rButtonExpert);
        panelOptions.add(rButtonCustom);
        panelOptions.add(panelCustomValues);

        panelMineCount.setLayout(new FlowLayout());
        panelMineCount.add(labelMinesLeft);
        panelMineCount.add(labelMineCount);

        GroupLayout panelMenuLayout = new GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(buttonStart, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelMenuLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(panelOptions, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(panelMineCount, GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap())
        );
        panelMenuLayout.setVerticalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panelMenuLayout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(buttonStart, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                                .addGap(59, 59, 59)
                                .addComponent(panelMineCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(59, 59, 59)
                                .addComponent(panelOptions, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(156, Short.MAX_VALUE))
        );

        scrollGrid.setViewportView(panelMain);
        scrollGrid.setMinimumSize(new Dimension(WIDTH,HEIGHT));
        scrollGrid.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        scrollGrid.getHorizontalScrollBar().setUnitIncrement(8);
        scrollGrid.getVerticalScrollBar().setUnitIncrement(8);
        panelMain.setLayout(new FlowLayout(FlowLayout.LEADING));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(panelMenu, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(scrollGrid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(scrollGrid, javax.swing.GroupLayout.DEFAULT_SIZE, HEIGHT, Short.MAX_VALUE)
                                .addContainerGap())
                        .addComponent(panelMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }
    public void updateMineCountDisplay(int curMineCount)
    {
        labelMineCount.setText(Integer.toString(curMineCount));
    }
    private int whichIsPressed()
    {
        if(rButtonBeginner.isSelected())
            return 1;
        if(rButtonIntermediate.isSelected())
            return 2;
        if(rButtonExpert.isSelected())
            return 3;
        if(rButtonCustom.isSelected())
            return 0;
        return -1;
    }
    private void showGrid()
    {
        Component[] compList = panelMain.getComponents();
        if(compList.length >0)
        {
            compList[0].setVisible(false);
            panelMain.remove(compList[0]);
        }

        panelMain.add(panelGrid, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }
    private void buttonStartActionPerformed(java.awt.event.ActionEvent evt)
    {
        int pressedID = whichIsPressed();
        if(pressedID != 0)
            engine.startGame(pressedID);
        else
        {
            try
            {
                int rows,cols,bombs;
                cols = Integer.parseInt(inputWidth.getText());
                rows = Integer.parseInt(inputHeight.getText());
                bombs = Integer.parseInt(inputMines.getText());
                if(cols < 8 || rows < 8 || bombs < 1 || cols > 30 || rows > 30)
                    JOptionPane.showMessageDialog(null,"ERROR: Grid must be between 8x8 and 30x30 and must have  at least 1 mine");
                else
                    if(bombs > rows * cols / 2)
                        JOptionPane.showMessageDialog(null,"ERROR: Too many mines");
                    else
                        engine.startGame(rows,cols,bombs);
            }
            catch (NumberFormatException e)
            {
                JOptionPane.showMessageDialog(null,"ERROR: Invalid custom values");
            }
        }
    }
    public void initializePanelGrid()
    {
        panelGrid = new GridPanel(engine);
        showGrid();
    }
    public void displayCellValue(int row, int col)
    {
        panelGrid.getCell(row,col).showValue();
    }
    public void winGame()
    {
        freezeGrid();
        JOptionPane.showMessageDialog(null,"YOU WON!(a.k.a. didn't blow up)");
    }
    public void loseGame(int explosionRow, int explosionCol)
    {
        displayAllBombs();
        panelGrid.getCell(explosionRow,explosionCol).explode();
        freezeGrid();
        JOptionPane.showMessageDialog(null,"YOU LOST (boom)");
    }
    private void freezeGrid()
    {
        panelGrid.disableAllCells();
    }
    private void displayAllBombs()
    {
        panelGrid.revealBombs();
    }
}
