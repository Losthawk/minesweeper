import java.awt.*;
public class MainClass
{
    public static void main(String[] args)
    {
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                MainGUI gui = new MainGUI();
                GridEngine engine = new GridEngine();
                gui.setEngine(engine);
                engine.setGui(gui);
                gui.setVisible(true);
            }
        });
    }
}
