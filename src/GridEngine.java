import java.util.Random;
import static java.lang.Math.abs;

public class GridEngine
{
    private int[][] grid;
    private int[][] userGrid;
    private int gridHeight;
    private int gridWidth;
    private int cellsLeft;
    private int currentMineCount;
    private static final int[] offsetY = {-1,-1,-1,0,0,1,1,1};
    private static final int[] offsetX = {-1,0,1,-1,1,-1,0,1};
    private MainGUI gui;

    private boolean isValidCell(int row, int col) {return !(row<0 || row>=gridHeight || col<0 || col>=gridWidth);}

    private boolean isCovered(int row, int col) {return (userGrid[row][col] == 0 );}

    private boolean isMarked(int row,int col) {return (userGrid[row][col] == -1);}

    private boolean isValidToOpen(int row,int col) {return(isCovered(row,col) && (!isMarked(row,col)));}

    private void checkWinCondition()
    {
        if(cellsLeft == 0)
            gui.winGame();
    }

    private void initializeGrid(int rows, int cols, int bombCount)
    {
        //Initialize values
        currentMineCount = bombCount;
        gridHeight = rows;
        gridWidth = cols;
        cellsLeft =gridWidth * gridHeight -bombCount;
        userGrid = new int[gridHeight][gridWidth];
        grid = new int[gridHeight][gridWidth];

        //Place *bombCount* mines on the grid (...randomly)
        Random random = new Random();
        int bomb = 0;
        while(bomb < bombCount)
        {
            int randomX = abs(random.nextInt() % gridWidth);
            int randomY = abs(random.nextInt() % gridHeight);
            if(grid[randomY][randomX] != -1)
            {
                grid[randomY][randomX] = -1;
                bomb++;
            }
        }

        //Calculate the cell numbers (representing the number of mines around)
        for(int i=0;i<gridHeight;i++)
            for(int j=0;j<gridWidth;j++)
            {
                if(grid[i][j] != -1)
                {
                    for(int pos=0;pos<8;pos++)
                        if(isValidCell(i+offsetY[pos],j+offsetX[pos]))
                        {
                            if(grid[i+ offsetY[pos]][j+ offsetX[pos]] == -1)
                                grid[i][j]++;
                        }
                }
            }
    }

    public void setUserCell(int row, int col, int value){userGrid[row][col] = value;}

    public boolean isMarkedWrong(int row, int col){return(grid[row][col] != -1 && userGrid[row][col] == -1);}

    public boolean isUnmarkedBomb(int row, int col){return(grid[row][col] == -1 && userGrid[row][col] != -1);}

    public int getGridValue(int row, int col){return grid[row][col];}

    public int getRowCount(){return grid.length;}

    public int getColCount(){return grid[0].length;}

    public void setGui(MainGUI guiObject) {gui = guiObject;}

    public int getCurrentMineCount() {return currentMineCount;}

    public void startGame(int difficulty)
    {
        switch (difficulty)
        {
            case 1:{startGame(8,8,10);break;}
            case 2:{startGame(16,16,40);break;}
            case 3:{startGame(30,16,99);break;}
            default:{startGame(30,30,199);break;}
        }
    }
    public void startGame(int height, int width, int bombCount)
    {
        initializeGrid(height,width,bombCount);
        gui.initializePanelGrid();
        updateMineCount(currentMineCount);
    }
    public void updateMineCount(int count)
    {
        currentMineCount = count;
        gui.updateMineCountDisplay(currentMineCount);
    }

    public void openCell(int row, int col)
    {
        userGrid[row][col] = 1;
        gui.displayCellValue(row,col);
        if(grid[row][col] == -1)
        { // BANG !!!
            gui.loseGame(row,col);
        }
        else
        { // LUCKY YOU !!!
            cellsLeft--;
            checkWinCondition();
            if(grid[row][col] == 0)
            {
                for(int pos=0;pos<8;pos++)
                    if(isValidCell(row+offsetY[pos],col+offsetX[pos]) && isValidToOpen(row+offsetY[pos],col+offsetX[pos]))
                    {
                        openCell(row + offsetY[pos], col + offsetX[pos]);
                    }
            }
        }
    }
}
